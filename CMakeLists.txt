# Top-level CMakeLists.txt for libLASi

# Copyright (C) 2007-2019 Alan W. Irwin
# Copyright (C) 2007-2011 Andrew Ross
# Copyright (C) 2007 Werner Smekal

# This file is part of libLASi.
#
# LibLASi is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# LibLASi is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with LibLASi; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA

# Version 3.13.2 or above of cmake is required!
CMAKE_MINIMUM_REQUIRED(VERSION 3.13.2)

project(libLASi CXX)

message(STATUS "CMake version = ${CMAKE_VERSION}")
message(STATUS "CMAKE_SYSTEM_NAME = ${CMAKE_SYSTEM_NAME}")

set(PACKAGE lasi)

# Location where libLASi cmake build system first looks for cmake modules.
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake/modules)

# Version information.

include(lasi_version)

enable_testing()

# parameters to control overall cmake behaviour.

# Configure libLASi component variables....

include(lasi)

# For windows put libLASi.dll in the dll subdirectory of the top-level
# build-tree directory to be consistent with other typical Windows
# builds, e.g., Plplot, ephcom2.

# You must add this directory to your PATH on Windows if you are using
# the (default) BUILD_SHARED_LIBS=ON option).
if(BUILD_SHARED_LIBS AND WIN32 AND NOT CYGWIN)
  set(LIBRARY_OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR}/dll)
endif(BUILD_SHARED_LIBS AND WIN32 AND NOT CYGWIN)

# Use configured variables to process configurable top-level files.

configure_file(
${CMAKE_CURRENT_SOURCE_DIR}/config.h.cmake 
${CMAKE_CURRENT_BINARY_DIR}/config.h
)

configure_file(
${CMAKE_CURRENT_SOURCE_DIR}/lasi.pc.in
${CMAKE_CURRENT_BINARY_DIR}/lasi.pc
)

install(FILES
${CMAKE_CURRENT_BINARY_DIR}/lasi.pc 
DESTINATION ${PKG_CONFIG_DIR}
)

install(FILES include/LASi.h DESTINATION ${includedir})

add_subdirectory(src)

add_subdirectory(examples)

#
# Generating documentation with doxygen
#
if(DOXYGEN_EXECUTABLE AND UNIX)
  
  # N.B. Both the following custom rules assume the doc directory exists
  # at make time, and the following install(DIRECTORY... must have doc exist
  # at cmake time.  Therefore, create the doc directory at CMake time.
  # (Linux experimentation indicates this is a no-op if the empty or
  # non-empty directory already exists.)
  file(MAKE_DIRECTORY ${CMAKE_SOURCE_DIR}/doc)

  # The initial rm command gets rid of everything previously built by this
  # custom command.
  add_custom_command(
  OUTPUT ${CMAKE_SOURCE_DIR}/doc/user/html/index.html
  COMMAND rm -rf ${CMAKE_SOURCE_DIR}/doc/user
  COMMAND mkdir ${CMAKE_SOURCE_DIR}/doc/user
  COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_SOURCE_DIR}/Doxyfile.user
  DEPENDS ${CMAKE_SOURCE_DIR}/Doxyfile.user
  WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  )

  # The initial rm command gets rid of everything previously built by this
  # custom command.
  add_custom_command(
  OUTPUT ${CMAKE_SOURCE_DIR}/doc/developer/html/index.html
  COMMAND rm -rf ${CMAKE_SOURCE_DIR}/doc/developer
  COMMAND mkdir ${CMAKE_SOURCE_DIR}/doc/developer
  COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_SOURCE_DIR}/Doxyfile.developer
  DEPENDS ${CMAKE_SOURCE_DIR}/Doxyfile.developer
  WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  )

  add_custom_target(
  documentation ALL
  DEPENDS
  ${CMAKE_SOURCE_DIR}/doc/user/html/index.html
  ${CMAKE_SOURCE_DIR}/doc/developer/html/index.html
  )
  
  # Install the documentation generated at "make" time.
  install(DIRECTORY ${CMAKE_SOURCE_DIR}/doc/ DESTINATION ${docdir}/html)

endif(DOXYGEN_EXECUTABLE AND UNIX)

#
# Packing stuff
#
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
"The PostScript Complex Text Layout Library, LASi")
set(CPACK_PACKAGE_VENDOR "Ed Trager <ed.trager@gmail.com>")
set(CPACK_PACKAGE_DESCRIPTION_FILE ${CMAKE_CURRENT_SOURCE_DIR}/README)
if(WIN32)
  set(CPACK_GENERATOR ZIP)
else(WIN32)
  set(CPACK_GENERATOR TGZ)
endif(WIN32)
set(
CPACK_SOURCE_PACKAGE_FILE_NAME
"libLASi-${VERSION}"
CACHE INTERNAL "tarball basename"
)
if(WIN32)
  set(CPACK_SOURCE_GENERATOR ZIP)
else(WIN32)
  set(CPACK_SOURCE_GENERATOR TGZ)
endif(WIN32)
# The following components are regex's to match anywhere (unless anchored)
# in absolute path + filename to find files or directories to be excluded
# from source tarball.
set(CPACK_SOURCE_IGNORE_FILES
"\\\\#.*$"
"~$"
"^${PROJECT_SOURCE_DIR}.*\\\\.svn/"
"^${PROJECT_SOURCE_DIR}/debian"
)
#message("CPACK_SOURCE_IGNORE_FILES = ${CPACK_SOURCE_IGNORE_FILES}")
include(CPack)
summary()
