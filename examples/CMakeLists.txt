# examples/CMakeLists.txt

# Copyright (C) 2007-2019 Alan W. Irwin
# Copyright (C) 2007 Andrew Ross

# This file is part of libLASi.
#
# LibLASi is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# LibLASi is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with LibLASi; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA

include_directories(${CMAKE_SOURCE_DIR}/include ${PANGOFT2_includedir})

set(CTEST_EXAMPLES_OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR}/ctest_examples_output_dir)
file(MAKE_DIRECTORY ${CTEST_EXAMPLES_OUTPUT_DIR})

set(SRC_ROOT_NAME_LIST
  MissingGlyphExample
  SimpleLASiExample
  ComplexTextLayoutExample
  )
  
find_program(INKSCAPE_EXECUTABLE inkscape)
message(STATUS "inkscape executable = ${INKSCAPE_EXECUTABLE}")
if(NOT INKSCAPE_EXECUTABLE)
  message(STATUS "Platform issue: inkscape command not available.")
  message(STATUS "Therefore example Encapsulated PostScript results cannot be transformed to PNG format")
endif(NOT INKSCAPE_EXECUTABLE)

set(INSTALL_LIST)
foreach(SRC_ROOT_NAME ${SRC_ROOT_NAME_LIST})

  list(APPEND INSTALL_LIST ${SRC_ROOT_NAME}.cpp)

  add_executable(${SRC_ROOT_NAME} ${SRC_ROOT_NAME}.cpp)
  target_link_libraries(${SRC_ROOT_NAME} LASi)

  add_test(${SRC_ROOT_NAME}
    ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME} ${CTEST_EXAMPLES_OUTPUT_DIR}/${SRC_ROOT_NAME}.eps
    )

  if(INKSCAPE_EXECUTABLE)
    add_custom_command(
      OUTPUT
      ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME}.eps
      ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME}.png
      COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME}.eps
      COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME}.png
      COMMAND ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME} > ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME}.eps
      COMMAND ${INKSCAPE_EXECUTABLE} --export-background=\#ffffff --file=${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME}.eps --export-png=${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME}.png
      DEPENDS ${SRC_ROOT_NAME}
      VERBATIM
      )
    add_custom_target(test_${SRC_ROOT_NAME} ALL
      DEPENDS
      ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME}.eps
      ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME}.png
      )
  else(INKSCAPE_EXECUTABLE)
    add_custom_command(
      OUTPUT
      ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME}.eps
      COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME}.eps
      COMMAND ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME} > ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME}.eps
      DEPENDS ${SRC_ROOT_NAME}
      VERBATIM
      )
    add_custom_target(test_${SRC_ROOT_NAME} ALL
      DEPENDS
      ${CMAKE_CURRENT_BINARY_DIR}/${SRC_ROOT_NAME}.eps
      )
  endif(INKSCAPE_EXECUTABLE)
    
endforeach(SRC_ROOT_NAME ${SRC_ROOT_NAME_LIST})

install(FILES
  ${INSTALL_LIST}
  README 
  MissingGlyphExample.png
  SimpleLASiExample.png
  ComplexTextLayoutExample.png
  DESTINATION ${DATA_DIR}/examples
  )

# Sort out RPATH issues for build of installed examples.
if(USE_RPATH)
  get_target_property(_LIB_INSTALL_RPATH LASi INSTALL_RPATH)
  string(REGEX REPLACE ";" ":" LIB_INSTALL_RPATH "${_LIB_INSTALL_RPATH}")
  set(RPATHCMD "-Wl,-rpath -Wl,${LIB_INSTALL_RPATH}")
endif(USE_RPATH)

# Sort out PKG_CONFIG_PATH issues for build of installed examples.

if("$ENV{PKG_CONFIG_PATH}" STREQUAL "")
  set(PKG_CONFIG_PATH "${libdir}/pkgconfig")
else("$ENV{PKG_CONFIG_PATH}" STREQUAL "")
  set(PKG_CONFIG_PATH "${libdir}/pkgconfig:$ENV{PKG_CONFIG_PATH}")
endif("$ENV{PKG_CONFIG_PATH}" STREQUAL "")
      
configure_file(
${CMAKE_CURRENT_SOURCE_DIR}/Makefile.examples.in
${CMAKE_CURRENT_BINARY_DIR}/Makefile.examples
)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/Makefile.examples
DESTINATION ${DATA_DIR}/examples
RENAME Makefile
)
