/** @file
 * Convert return-code of Freetype library calls to std::runtime_error.
 * Usage: place every call to Freetype library inside FtEval::eval().
 *
 * libLASi provides a C++ output stream interface for writing 
 * multi-language Postscript documents.
 * Copyright (C) 2003, 2004 Larry Siden.
 * 
 * See README file in project root directory for copyright and contact info.
 * See COPYING file in project root for terms of re-distribution.
 */

#ifndef GLYPH_MANAGER_H
#define GLYPH_MANAGER_H

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

/** Manage FT_Glyph by insuring that FT_Glyph is handled correctly.
 */
class FreetypeGlyphMgr { //: public Manager<FT_GlyphRec*> {
  private:
    FT_Glyph _glyph;

  public:
    FreetypeGlyphMgr();
    FreetypeGlyphMgr(FT_Glyph glyph);
    FreetypeGlyphMgr(const FreetypeGlyphMgr& ftgm);
    FreetypeGlyphMgr& operator=(const FreetypeGlyphMgr& ftgm);
    ~FreetypeGlyphMgr();
    operator FT_Glyph() const;
    void assign(const FT_Glyph glyph);
};
#endif
