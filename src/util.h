/** @file
 * Convert return-code of Freetype library calls to std::runtime_error.
 * Usage: place every call to Freetype library inside FtEval::eval().
 *
 * libLASi provides a C++ output stream interface for writing multi-language Postscript documents.
 * Copyright (C) 2003, 2004 by Larry Siden.
 *
 * See README file in project root directory for copyright and contact info.
 * See COPYING file in project root for terms of re-distribution.
 */

#ifndef UTIL_H
#define UTIL_H

#include <stdexcept>
#include <string>
#include <iostream>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

std::ostream& operator<<(std::ostream&, const FT_Library);
std::ostream& operator<<(std::ostream&, const FT_Face);
std::ostream& operator<<(std::ostream&, const FT_Glyph);
std::ostream& operator<<(std::ostream&, const FT_Outline);

/** Converts a freetype return code into an exception.
 */
inline void evalReturnCode(const int errCode, const char* funcName) throw (std::runtime_error) {
  if (errCode)
    throw std::runtime_error(std::string("Error returned from ") + funcName);
}
#endif
